package pageObjectModel.pomCore.commonUtils;

import pageObjectModel.pomCore.coreUtils.PropertyManager;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectivityStatus {

    private String requestedUrl;
    private static int invalidLinkCount = 0;

    public boolean isNodeServerWorking(String deviceName) throws IOException {

        String nodeServerEmulator = PropertyManager.NODE_SERVER_URL_EMULATOR + "/status";
        String nodeServerDevice1 = PropertyManager.NODE_SERVER_URL_DEVICE1 + "/status";

        if (deviceName.equals(PropertyManager.EMULATOR_NAME)) {
            requestedUrl = nodeServerEmulator;
        }
        if (deviceName.equals(PropertyManager.DEVICE1_NAME)) {
            requestedUrl = nodeServerDevice1;
        }
        return isUrlWorking(requestedUrl);
    }

    public static boolean isUrlWorking(String url) {

        if(url == null) {
            System.out.println("!!! Caution >> Url value received in 'isUrlWorking()' method is: null");
            return false;
        } else {
            try {
                URL linkUrl = new URL(url);
                HttpURLConnection urlConnection = (HttpURLConnection) linkUrl.openConnection();
                urlConnection.setConnectTimeout(5000);
                urlConnection.connect();
                int httpResponseCode = urlConnection.getResponseCode();
                if (httpResponseCode == 200) {
                    System.out.println("Requested Url : " + url + " responded with : " + httpResponseCode + " : " + urlConnection.getResponseMessage());
                    //logger.debug("Requested Url : " + url + " responded with : " + httpResponseCode + " : " + urlConnection.getResponseMessage());
                } else if (httpResponseCode == 404){
                    System.out.println("Requested Url : " + url + " responded with : " + httpResponseCode + " : " + urlConnection.getResponseMessage());
                    // Incrementing the invalidLink count in case of a Broken Link.
                    invalidLinkCount++;
                } else {
                    System.out.println("Requested Url : " + url + " responded with : " + httpResponseCode + " : " + urlConnection.getResponseMessage());
                    invalidLinkCount++;
                }
                return (httpResponseCode == 200);
            } catch (IOException e) {
                System.out.println("Error in execution of 'isUrlWorking()' method for : " + url);
                invalidLinkCount++;
                e.printStackTrace();
            }
            return (false);
        }
    }

    public static int getInvalidLinkCount() {
        System.out.println("Total No of Invalid Links found: " + invalidLinkCount);
        return invalidLinkCount;
    }
}