package pageObjectModel.pomCore.commonUtils;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class BrokenLinks {

    private static int absoluteUrlCount = 0;

    /**
     * @param androidDriver
     * @param url of the webpage on which test will be performed
     * @return the no of invalid/broken urls present on the webpage
     * @throws RuntimeException
     */

    public static int checkBrokenLinks(AndroidDriver<WebElement> androidDriver, String url) throws RuntimeException {

        // Find all the elements/links on the webpage with an 'a' tag
        List<WebElement> allLinks = androidDriver.findElements(By.tagName("a"));

        // Iterate through all the available links on the webpage
        for (WebElement link : allLinks) {
            String linkURL = link.getAttribute("href");
            if(linkURL != null && linkURL.startsWith("http")) {
                absoluteUrlCount++;
                System.out.println("Url to be sent to 'isUrlWorking()' method is: " + linkURL);
                ConnectivityStatus.isUrlWorking(linkURL);
            }
        }
        System.out.println("\nTotal no. of Absolute Urls found on: (" + url +"): " + absoluteUrlCount);
        int brokenLinkCount = ConnectivityStatus.getInvalidLinkCount();
        System.out.println("\nTotal no. of Invalid Urls found on: (" + url +"): " + brokenLinkCount);
        return brokenLinkCount;
    }
}