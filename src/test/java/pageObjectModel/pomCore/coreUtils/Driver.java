package pageObjectModel.pomCore.coreUtils;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Optional;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    public AndroidDriver<WebElement> androidDriver = null;
    public WebDriverWait wdWait;

    private DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    private String appiumNodeServerUrl;

    final String defaultDeviceName = "emulator";


    // Below method accepts an @Optional argument i.e. deviceName,
    // if it is not passed by the calling method 'defaultDeviceName' will be used for "deviceName" capability.
    public AndroidDriver<WebElement> MobileCapabilities(@Optional(defaultDeviceName) String deviceName) throws MalformedURLException {

        if(deviceName.equals(PropertyManager.EMULATOR_NAME)) {
            appiumNodeServerUrl = PropertyManager.NODE_SERVER_URL_EMULATOR;
            desiredCapabilities.setCapability("deviceName", PropertyManager.EMULATOR_NAME);
            desiredCapabilities.setCapability("udid", PropertyManager.EMULATOR_UDID);
        }

        if(deviceName.equals(PropertyManager.DEVICE1_NAME)) {
            appiumNodeServerUrl = PropertyManager.NODE_SERVER_URL_DEVICE1;
            desiredCapabilities.setCapability("deviceName", PropertyManager.DEVICE1_NAME);
            desiredCapabilities.setCapability("udid", PropertyManager.DEVICE1_UDID);
        }

        desiredCapabilities.setCapability("platformName", PropertyManager.PLATFORM_NAME);
        desiredCapabilities.setCapability("browserName", PropertyManager.BROWSER_NAME);
        desiredCapabilities.setCapability("unicodeKeyboard", true);
        desiredCapabilities.setCapability("resetKeyboard", true);

        // If 'True', writes the Chromedriver logs inline with the Appium logs (use for debugging purposes)
        desiredCapabilities.setCapability("showChromedriverLog", false);
        // If 'True', bypasses automatic Chromedriver configuration and uses the version that comes downloaded with Appium
        desiredCapabilities.setCapability("chromedriverUseSystemExecutable", true);

        androidDriver = new AndroidDriver<>(new URL(appiumNodeServerUrl), desiredCapabilities);
        wdWait = new WebDriverWait(androidDriver, PropertyManager.DEFAULT_EXPLICIT_WAIT);
        return androidDriver;
    }
}