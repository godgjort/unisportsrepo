package pageObjectModel.pomCore.coreUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    private static FileReader fileReader;

    public static String HOME_PAGE_URL;
    public static String EMULATOR_NAME;
    public static String EMULATOR_UDID;
    public static String DEVICE1_NAME;
    public static String DEVICE1_UDID;
    public static String PLATFORM_NAME;
    public static String BROWSER_NAME;
    public static String NODE_SERVER_URL_EMULATOR;
    public static String NODE_SERVER_URL_DEVICE1;

    public static int DEFAULT_EXPLICIT_WAIT;

    private static void configFileReader() throws IOException {
        File propertiesFile = new File(System.getProperty("user.dir")+
                "/src/test/resources/properties/configuration.properties");
        fileReader = new FileReader(propertiesFile);
    }

    public static void loadAutomationProperties() {

        Properties configurationProperties = new Properties();

        try { configFileReader(); }
        catch (IOException e) { e.printStackTrace(); }

        try {
            configurationProperties.load(fileReader);
            fileReader.close();
        }
        catch (IOException e) { e.printStackTrace(); }

        try {
            HOME_PAGE_URL =  configurationProperties.getProperty("homePageUrl");
            EMULATOR_NAME =  configurationProperties.getProperty("emulatorName");
            EMULATOR_UDID = configurationProperties.getProperty("emulatorUdid");
            DEVICE1_NAME = configurationProperties.getProperty("device1Name");
            DEVICE1_UDID = configurationProperties.getProperty("device1Udid");
            PLATFORM_NAME = configurationProperties.getProperty("platformName");
            BROWSER_NAME = configurationProperties.getProperty("browserName");
            NODE_SERVER_URL_EMULATOR = configurationProperties.getProperty("nodeServerUrlEmulator");
            NODE_SERVER_URL_DEVICE1 = configurationProperties.getProperty("nodeServerUrlDevice1");
            DEFAULT_EXPLICIT_WAIT = Integer.parseInt(configurationProperties.get("defaultExplicitWait").toString());
        } catch (Exception e) {
            System.out.println("Error in Assigning/Fetching the values from 'configurationProperties'");
            e.printStackTrace();
        }
    }
}