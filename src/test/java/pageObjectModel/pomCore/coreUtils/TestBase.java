package pageObjectModel.pomCore.coreUtils;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;

import java.io.IOException;
import java.net.MalformedURLException;

public class TestBase  extends Driver {

    public AndroidDriver<WebElement> androidDriver;

    @Parameters({"deviceName"})
    @BeforeTest
    public void setUpBeforeTest(String deviceName) throws IOException {

        // Loading configuration properties here, so these will be available to all the tests
        PropertyManager.loadAutomationProperties();

        // Check if Appium node is connected/responding
        ConnectivityStatus connectivityStatus = new ConnectivityStatus();
        boolean isAppiumNodeWorking = connectivityStatus.isNodeServerWorking(deviceName);

        if (!isAppiumNodeWorking) {
            System.out.println("!!! Appium Node Server for " + deviceName + " is not working !!!");
        } else {
            System.out.println("Appium Node Server for " + deviceName + " is working");
        }
    }

    @Parameters({"deviceName"})
    @BeforeClass
    public void setUpBeforeClass(String deviceName) throws MalformedURLException {
        System.out.println("! ! ! ! ! Instantiating androidDriver @BeforeClass ! ! ! ! !");

        // Instantiating an object of 'AndroidCapabilities()' class.
        androidDriver = MobileCapabilities(deviceName);
    }

    @AfterClass
    public void tearDownAfterClass() {
        System.out.println("* * * * * Quitting androidDriver @AfterClass * * * * *");
        androidDriver.quit();
    }

    @AfterTest
    public void tearDownAfterTest() {
    }
}
