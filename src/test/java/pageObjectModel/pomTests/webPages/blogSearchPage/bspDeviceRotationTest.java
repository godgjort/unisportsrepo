package pageObjectModel.pomTests.webPages.blogSearchPage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.ScreenOrientation;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;
import pageObjectModel.pomRepository.OmrBlogSearchPage;

public class bspDeviceRotationTest extends TestBase {

    /**
     * This TestCase is aimed to verify that search results remain the same
     * after device rotation.
     */

    @Test
    public void VerifyDeviceRotationResults(){

        String searchKeyword = "fcb";
        String blogSearchPageUrl = PropertyManager.HOME_PAGE_URL+"/blog/search";
        boolean isBlogSearchPageUrlWorking = ConnectivityStatus.isUrlWorking(blogSearchPageUrl);

        if(isBlogSearchPageUrlWorking) {

            // Open Browser and launch 'blogPageUrl'
            androidDriver.get(blogSearchPageUrl);

            // Creating an Instance of 'omrBlogSearchPage' Class, passing 'androidDriver' as an argument
            OmrBlogSearchPage omrBlogSearchPage = new OmrBlogSearchPage(androidDriver);

            // Perform Test Steps
            omrBlogSearchPage.blogSearchPageInputTextSearch.clear();
            omrBlogSearchPage.blogSearchPageInputTextSearch.sendKeys(searchKeyword);
            omrBlogSearchPage.blogSearchPageIconSearch.click();
            androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));

            int searchResultCount = omrBlogSearchPage.totalSearchResultCount();
            System.out.println("Before rotation searchResultCount : " + searchResultCount);

            // change the device rotation
            if(androidDriver.getOrientation() == ScreenOrientation.PORTRAIT) {
                androidDriver.rotate(ScreenOrientation.LANDSCAPE);
            } else {
                androidDriver.rotate(ScreenOrientation.PORTRAIT);
            }
            System.out.println("After rotation searchResultCount : " + searchResultCount);

            // Verify that count for Search Items displayed remains the same before and after the device rotation
            Assert.assertEquals(searchResultCount, omrBlogSearchPage.totalSearchResultCount());

            // Resetting ScreenOrientation to PORTRAIT mode
            androidDriver.rotate(ScreenOrientation.PORTRAIT);
        }
    }
}
