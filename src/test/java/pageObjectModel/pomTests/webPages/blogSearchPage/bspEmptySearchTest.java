package pageObjectModel.pomTests.webPages.blogSearchPage;

import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.ScreenOrientation;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;
import pageObjectModel.pomRepository.OmrBlogPage;
import pageObjectModel.pomRepository.OmrBlogSearchPage;

public class bspEmptySearchTest extends TestBase {

    /**
     * This TestCase is aimed to verify that performing an Empty search
     * will result in displaying a proper error message.
     */

    @Test
    public void VerifyEmptySearchResults(){

        String blogSearchPageUrl = PropertyManager.HOME_PAGE_URL+"/blog/search";
        boolean isBlogSearchPageUrlWorking = ConnectivityStatus.isUrlWorking(blogSearchPageUrl);

        if(isBlogSearchPageUrlWorking) {

            // Open Browser and launch 'blogPageUrl'
            androidDriver.get(blogSearchPageUrl);

            // Creating an Instance of 'omrBlogSearchPage' Class, passing 'androidDriver' as an argument
            OmrBlogSearchPage omrBlogSearchPage = new OmrBlogSearchPage(androidDriver);

            // Perform Test Steps
            omrBlogSearchPage.blogSearchPageInputTextSearch.clear();
            omrBlogSearchPage.blogSearchPageIconSearch.click();

            boolean isEmptySearchResultCorrect = omrBlogSearchPage.blogSearchPageTextNoSearchResults.isDisplayed();
            System.out.println("Search results displayed for an Empty/Blank search text are correct : " + isEmptySearchResultCorrect);

            // Verify that Empty search results into a proper Error message.
            Assert.assertTrue(isEmptySearchResultCorrect);
        }
    }
}