package pageObjectModel.pomTests.webPages.blogHomePage;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.BrokenLinks;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;

public class bhpLinksTest extends TestBase {

    /**
     * This TestCase is aimed to verify that all the 'href' links present on the BlogPage are working fine.
     */

    @Test
    public void VerifyBlogPageLinks() {

        String blogPageUrl = PropertyManager.HOME_PAGE_URL+"/blog/";

        // Check if blogPageUrl is working
        boolean isBlogPageUrlWorking = ConnectivityStatus.isUrlWorking(blogPageUrl);

        if(isBlogPageUrlWorking) {
            androidDriver.get(blogPageUrl);
            int invalidUrlCount = BrokenLinks.checkBrokenLinks(androidDriver, blogPageUrl);
            Assert.assertEquals(invalidUrlCount, 0);
        }
    }
}

