package pageObjectModel.pomTests.webPages.blogHomePage;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;
import pageObjectModel.pomRepository.OmrBlogPage;
import pageObjectModel.pomRepository.OmrBlogSearchPage;

public class bhpSearchKeywordTest extends TestBase {

    /**
     * This TestCase is aimed to verify that 'Search Text Box' (of search result page)
     * keeps the searched keyword after the Search is complete.
     */

    @Test
    public void VerifyBlogSearchValidKeyword (){

        String searchKeyword = "messi";
        String blogPageUrl = PropertyManager.HOME_PAGE_URL+"/blog/";
        boolean isBlogPageUrlWorking = ConnectivityStatus.isUrlWorking(blogPageUrl);

        if(isBlogPageUrlWorking) {

            // Open Browser and launch 'blogPageUrl'
            androidDriver.get(blogPageUrl);

            // Creating an Instance of 'OmrBlogPage' Class, passing 'androidDriver' as an argument
            OmrBlogPage omrBlogPage = new OmrBlogPage(androidDriver);

            // Creating an Instance of 'omrBlogSearchPage' Class, passing 'androidDriver' as an argument
            OmrBlogSearchPage omrBlogSearchPage = new OmrBlogSearchPage(androidDriver);

            omrBlogPage.blogPageSidebar.click();
            omrBlogPage.blogPageInputTextSearch.click();
            omrBlogPage.blogPageInputTextSearch.clear();
            omrBlogPage.blogPageInputTextSearch.sendKeys(searchKeyword);
            androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));

            String searchedText = omrBlogSearchPage.blogSearchPageInputTextSearch.getAttribute("value");

            // Verify that searched keyword remain present in the 'Search Text Box'
            Assert.assertEquals(searchKeyword, searchedText);
        }
    }
}
