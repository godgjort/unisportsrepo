package pageObjectModel.pomTests.webPages.blogHomePage;

import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;
import pageObjectModel.pomRepository.OmrBlogPage;
import pageObjectModel.pomRepository.OmrBlogSearchPage;

public class bhpSearchResultsTest extends TestBase {

    /**
     * This TestCase is aimed to verify that search results are relevent i.e.
     * each displayed search result conatins the actual 'searchKeyword' in it.
     */

    @Test
    public void VerifySearchResults(){

        String searchKeyword = "fcb";
        String blogPageUrl = PropertyManager.HOME_PAGE_URL+"/blog/";
        boolean isBlogPageUrlWorking = ConnectivityStatus.isUrlWorking(blogPageUrl);

        if(isBlogPageUrlWorking) {

            // Open Browser and launch 'blogPageUrl'
            androidDriver.get(blogPageUrl);

            // Creating an Instance of 'OmrBlogPage' Class, passing 'androidDriver' as an argument
            OmrBlogPage omrBlogPage = new OmrBlogPage(androidDriver);
            // Creating an Instance of 'omrBlogSearchPage' Class, passing 'androidDriver' as an argument
            OmrBlogSearchPage omrBlogSearchPage = new OmrBlogSearchPage(androidDriver);

            omrBlogPage.blogPageSidebar.click();
            omrBlogPage.blogPageInputTextSearch.click();
            omrBlogPage.blogPageInputTextSearch.clear();
            omrBlogPage.blogPageInputTextSearch.sendKeys(searchKeyword);
            androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));

            // Verify that only the relevent search results are displayed.
            Assert.assertTrue(omrBlogSearchPage.verifySearchResults(searchKeyword));
        }
    }
}
