package pageObjectModel.pomTests.webPages.homePage;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjectModel.pomCore.commonUtils.BrokenLinks;
import pageObjectModel.pomCore.commonUtils.ConnectivityStatus;
import pageObjectModel.pomCore.coreUtils.PropertyManager;
import pageObjectModel.pomCore.coreUtils.TestBase;

public class HomePageLinksTest extends TestBase {

    /**
     * This TestCase is aimed to verify that all the 'href' links present on the HomePage are working fine.
     */

    @Test
    public void VerifyHomePageLinks() {

        String homePageUrl = PropertyManager.HOME_PAGE_URL;

        // Check if homePageUrl is working
        boolean isHomePageUrlWorking = ConnectivityStatus.isUrlWorking(homePageUrl);

        if(isHomePageUrlWorking) {
            androidDriver.get(homePageUrl);
            int invalidUrlCount = BrokenLinks.checkBrokenLinks(androidDriver, homePageUrl);
            Assert.assertEquals(invalidUrlCount, 0);
        }
    }
}
