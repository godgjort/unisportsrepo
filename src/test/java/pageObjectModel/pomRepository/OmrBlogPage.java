package pageObjectModel.pomRepository;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * OmrBlogPage is an Object and Methods (generic) repository for Blog webpage.
 **/

public class OmrBlogPage {

    private AndroidDriver<WebElement> androidDriver;

    //Constructor
    public OmrBlogPage(AndroidDriver<WebElement> driver) {
        this.androidDriver = driver;
        PageFactory.initElements(androidDriver, this);
    }

    //Locators
    @FindBy(xpath = "//div[contains(@class, '_2-3NoMrrCx3SZdpMb5m6iF')]")
    public WebElement blogPageSidebar;

    @FindBy(xpath = "//input[contains(@class, '_2wd9gLhmkYlXBxVhBXNH6i')]")
    public WebElement blogPageInputTextSearch;
}