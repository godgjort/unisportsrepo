package pageObjectModel.pomRepository;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OmrBlogSearchPage {

    private AndroidDriver<WebElement> androidDriver;
    private int incorrectSearchResultsCount = 0;
    private List<WebElement> allSearchResults;

    //Constructor
    public OmrBlogSearchPage(AndroidDriver<WebElement> driver) {
        this.androidDriver = driver;
        PageFactory.initElements(androidDriver, this);
    }

    //Locators
    @FindBy(xpath="//input[contains(@class, '_3Hr3qONzU09D2EpcszWrzd')]")
    public WebElement blogSearchPageInputTextSearch;

    @FindBy(xpath="//button[contains(@class, '_yufu0rNaN')]")
    public WebElement blogSearchPageIconSearch;

    @FindBy(xpath="//h1[contains(@class, '_3j1YMd3zjL1xNt87XF8hqO')]")
    public WebElement blogSearchPageTextNoSearchResults;

    @FindBy(xpath="//div[@class='qq-Pyd4MklQfqSfWC1B5j']/a")
    public List<WebElement> anchorsInSearchResult;

    public boolean verifySearchResults(String searchText) {

        androidDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        allSearchResults = anchorsInSearchResult;
//        List<WebElement> allSearchResults = androidDriver.findElements(By.xpath("//div[@class='qq-Pyd4MklQfqSfWC1B5j']/a"));
        List<String> incorrectSearchResults = new ArrayList<>();

        if(allSearchResults.size() > 0) {
            // Iterate through all the available results on the webpage
            for (WebElement searchResult : allSearchResults) {
//                System.out.println(searchResult.getText());
                String searchResultText = searchResult.getText();
                if(searchResultText != null && searchResultText.toLowerCase().contains(searchText)) {
//                    System.out.println("Relevent search result is displayed for '" + searchText + "'");
                } else {
                    incorrectSearchResults.add(searchResult.getAttribute("href"));
                    incorrectSearchResultsCount++;
                }
            }
        }
        boolean isIncorrectSearchFound = (incorrectSearchResultsCount == 0);
        System.out.println("\nTotal no. of incorrectSearchResults found : " + incorrectSearchResultsCount);
        System.out.println("Below is the list of Searches missing the Search keyword (if any) : ");
        if(isIncorrectSearchFound) {
            System.out.println(" - none found - \n");
        } else{
            incorrectSearchResults.forEach(System.out::println);
        }
        return (isIncorrectSearchFound);
    }

    // totalSearchResultCount: will return the total count of search items/blogs received for a given keyword
    public int totalSearchResultCount() {
        androidDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        allSearchResults = anchorsInSearchResult;
        System.out.println("Inside 'totalSearchResultCount()' and value for allSearchResults is : " + allSearchResults.size());
        return allSearchResults.size();
    }
}
